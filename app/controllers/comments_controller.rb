class CommentsController < ApplicationController
	before_action :set_comment, only: [:show, :edit, :update, :destroy]
 
	def index
    	@comments = Comment.all
    	# @articles = @comment.articles.create(order_date: Time.now)
  	end

	def show
		@articles = Article.all
	end

	def new
		@comment = Comment.new
		@articles = Article.all
	end

	def edit
		@articles = Article.all
	end

	 def create
	    @comment = Comment.new(comment_params)
	    @articles = Article.all

	    respond_to do |format|
	      if @comment.save
	        format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
	        format.json { render :show, status: :created, location: @comment }
	      else
	        format.html { render :new }
	        format.json { render json: @comment.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def update
		respond_to do |format|
			if @comment.update(comment_params)
				format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
				format.json { render :show, status: :ok, location: @comment }
			else
				format.html { render :edit }
				format.json { render json: @comment.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@comment.destroy
		respond_to do |format|
      	format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      	format.json { head :no_content }
    	end
	end
	
	private
	    def set_comment
      		@comment = Comment.find(params[:id])
    	end

	    def comment_params
	    params.require(:comment).permit(:commenter, :body, :article_id)
	end
end
